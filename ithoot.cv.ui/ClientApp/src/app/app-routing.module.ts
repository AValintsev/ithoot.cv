import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IthootResumeComponent} from './ithoot-resume/ithoot-resume.component';
import {AuthGuard} from "../code/guards";
import {LoginComponent} from "./ithoot-resume/authorization/login/login.component";
import {AuthorizationModule} from "./ithoot-resume/authorization/authorization.module";
import {RegistrationComponent} from "./ithoot-resume/authorization/registration/registration.component";

const routes: Routes = [
  {
    path: '',
    component: IthootResumeComponent,
    children: [
      {
        path: '',
        loadChildren: './ithoot-resume/about/about.module#AboutModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'curriculum',
        loadChildren: './ithoot-resume/curriculum/curriculum.module#CurriculumModule'
      },
      {
        path: 'cv-editor',
        loadChildren: './ithoot-resume/cv-editor/cv-editor.module#CvEditorModule'
      },
      {
        path: 'layouts',
        loadChildren: './ithoot-resume/layouts/layouts.module#LayoutsModule'
      },
      {
        path: 'cv-list',
        loadChildren: './ithoot-resume/cv-list/cv-list.module#CvListModule'
      },
      {
        path: 'cl-editor',
        loadChildren: './ithoot-resume/cl-editor/cl-editor.module#ClEditorModule'
      },
      {
        path: 'cv-new',
        loadChildren: './ithoot-resume/cv-new/cv-new.module#CvNewModule'
      }

    ],
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes),
      AuthorizationModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
