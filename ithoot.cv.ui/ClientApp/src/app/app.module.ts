import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {MDBBootstrapModule} from "angular-bootstrap-md";
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {IthootResumeComponent} from './ithoot-resume/ithoot-resume.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MustHaveComponentsModule} from "./ithoot-resume/must-have-components";
import {MatProgressBarModule, MatSliderModule} from "@angular/material";
import {QuillModule} from "ngx-quill";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from "@angular/common/http";
import { JwtHelperService, JWT_OPTIONS } from "@auth0/angular-jwt";

@NgModule({
    declarations: [
        AppComponent,
        IthootResumeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MDBBootstrapModule.forRoot(),
        BrowserAnimationsModule,
        MatProgressBarModule,
        MatSliderModule,
        MustHaveComponentsModule,
        ReactiveFormsModule,
        QuillModule,
        FormsModule,
        NgbModule,
        HttpClientModule
    ],
    providers: [
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
        JwtHelperService
    ],
    bootstrap: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
}
