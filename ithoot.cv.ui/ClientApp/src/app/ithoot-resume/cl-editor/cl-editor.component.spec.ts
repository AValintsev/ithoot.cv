import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClEditorComponent } from './cl-editor.component';

describe('ClEditorComponent', () => {
  let component: ClEditorComponent;
  let fixture: ComponentFixture<ClEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
