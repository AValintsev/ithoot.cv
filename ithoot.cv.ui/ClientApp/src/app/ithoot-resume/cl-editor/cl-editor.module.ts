import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ClEditorComponent} from "./cl-editor.component";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
      ClEditorComponent
  ],
  imports: [
    CommonModule,
      RouterModule.forChild([{
        path:'',
        component: ClEditorComponent
      }])
  ]
})
export class ClEditorModule { }
