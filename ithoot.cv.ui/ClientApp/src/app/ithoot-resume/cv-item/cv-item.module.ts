import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CvItemComponent} from './cv-item.component';
import {RouterModule} from '@angular/router';
import {ButtonsModule, CardsFreeModule, WavesModule} from 'angular-bootstrap-md'

@NgModule({
    declarations: [
        CvItemComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        ButtonsModule,
        WavesModule,
        CardsFreeModule
    ],
    exports: [
        CvItemComponent
    ]
})
export class CvItemModule {
}
