import {Component, Input, OnInit} from '@angular/core';
import {PersonInfo} from '../../../code/models/person-info';


@Component({
    selector: 'app-cv-item',
    templateUrl: './cv-item.component.html',
    styleUrls: ['./cv-item.component.scss']
})
export class CvItemComponent implements OnInit {

    @Input('data') person: PersonInfo;

    constructor() {
    }

    ngOnInit() {
    }
}
