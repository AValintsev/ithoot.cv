import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Education, Experience, Language, PersonInfo, Skill} from '../../../code/models/person-info';
import {CurriculumService} from '../../../code/services/curriculum.service';


@Component({
    selector: 'app-cv-editor',
    templateUrl: './cv-editor.component.html',
    styleUrls: ['./cv-editor.component.scss']
})

export class CvEditorComponent implements OnInit {
    // model;
    public person: PersonInfo;
    public languages;
    public languageLevels = ['Proficient', 'Advanced', 'Upper-intermediate', 'Intermediate', 'Pre-intermediate', 'Elementary', 'Beginner/False beginner', 'Native'];
    public experiences;
    public education;
    public skills;
    public personPhoto = '';


    validationForm: FormGroup;
    editorForm: FormGroup;


    editorStyle = {
        fontSize: '16px',
        backgroundColor: '#f7f7f7'

    };

    config = {
        toolbar: [
            ['bold', 'italic', 'underline'],
            ['link', 'image']
        ]
    };

    configCustom = {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
            ['blockquote', 'code-block'],
            [{'list': 'ordered'}, {'list': 'bullet'}],
            [{'script': 'sub'}, {'script': 'super'}],
            [{'align': []}],
            ['clean'],
            ['link', 'image']
        ]
    };


    constructor(
        private cvService: CurriculumService,
        public fb: FormBuilder
    ) {

        this.validationForm = fb.group({
            emailFormEx: [null, [Validators.required, Validators.email]],
            passwordFormEx: [null, Validators.required],
        });
    }

    ngOnInit() {
        this.person = this.cvService.getPerson(1);
        this.languages = this.person.languages;
        this.experiences = this.person.experiences;
        this.education = this.person.education;
        this.skills = this.person.skills;
        this.personPhoto = '../../../assets/images/' + this.person.photo;

        this.editorForm = new FormGroup({
            'editor': new FormControl(null)
        })
    }


    get emailFormEx() {
        return this.validationForm.get('emailFormEx');
    }

    onSelectFile(event: any) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();

            reader.readAsDataURL(event.target.files[0]); // read file as data url

            reader.onload = (event) => { // called once readAsDataURL is completed
                this.personPhoto = event.target['result'];
                console.log(this.personPhoto);
            }
        }
    }

    addLanguage() {
        this.languages.unshift(new Language());
    }

    removeLanguage(index) {
        this.languages.splice(index, 1);
    }

    addExperience() {
        this.experiences.unshift(new Experience());
    }

    removeExperience(index) {
        this.experiences.splice(index, 1);
    }

    addEducation() {
        this.education.unshift(new Education());
    }

    removeEducation(index) {
        this.education.splice(index, 1);
    }

    addSkill() {
        this.skills.unshift(new Skill())
    }

    removeSkill(index) {
        this.skills.splice(index, 1);
    }

}
