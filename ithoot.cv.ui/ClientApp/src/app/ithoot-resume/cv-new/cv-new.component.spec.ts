import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CvNewComponent } from './cv-new.component';

describe('CvNewComponent', () => {
  let component: CvNewComponent;
  let fixture: ComponentFixture<CvNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CvNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CvNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
