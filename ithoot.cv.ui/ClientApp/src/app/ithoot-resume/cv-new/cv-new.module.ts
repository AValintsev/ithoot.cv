import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CvNewComponent} from './cv-new.component';
import {RouterModule} from '@angular/router';
import {
    ButtonsModule,
    CollapseModule,
    IconsModule,
    InputsModule,
    InputUtilitiesModule,
    WavesModule
} from 'angular-bootstrap-md';
import {
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatSelectModule,
    MatSliderModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {QuillModule} from 'ngx-quill';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DatepickerModule} from '../datepicker/datepicker.module';
import {EducateDatepickerModule} from '../educate-datepicker/educate-datepicker.module';
import {library} from '@fortawesome/fontawesome-svg-core';
import {
    faAddressCard,
    faClone,
    faCoffee,
    faEnvelope,
    faFile,
    faHome,
    faImage,
    faMapMarkerAlt,
    faMinus,
    fas,
    faTimes,
    faTrashAlt,
    faUser
} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';

library.add(faCoffee, faHome, faEnvelope, faUser, faFile, faAddressCard, faClone, faImage, faMinus, faTimes, faMapMarkerAlt, faTrashAlt);
library.add(fas, far);


@NgModule({
    declarations: [
        CvNewComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([{
            path: '', component: CvNewComponent
        }]),
        IconsModule,
        MatProgressBarModule,
        MatSliderModule,
        ReactiveFormsModule,
        FormsModule,
        QuillModule,
        ButtonsModule,
        WavesModule,
        CollapseModule,
        FontAwesomeModule,
        InputsModule.forRoot(),
        MatSelectModule,
        InputUtilitiesModule,
        MatDatepickerModule,
        MatNativeDateModule,
        NgbModule,
        MatInputModule,
        DatepickerModule,
        EducateDatepickerModule
    ]
})
export class CvNewModule {
}
