import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Education, Experience, Language, PersonInfo, Skill} from "../../../code/models/person-info";


@Component({
    selector: 'app-cv-new',
    templateUrl: './cv-new.component.html',
    styleUrls: ['./cv-new.component.scss'],

})
export class CvNewComponent implements OnInit {

    public person: PersonInfo;
    public languages = [];
    public languageLevels = ['Proficient', 'Advanced', 'Upper-intermediate', 'Intermediate', 'Pre-intermediate', 'Elementary', 'Beginner/False beginner', 'Native'];
    public experiences = [];
    public education = [];
    public skills = [];
    public personPhoto = '';


    validationForm: FormGroup;
    editorForm: FormGroup;
    editorContent: string;

    editorStyle = {
        fontSize: '16px',
        backgroundColor: '#f7f7f7',
        height: '150px'
    };

    config = {
        toolbar: [
            ['bold', 'italic', 'underline'],
            ['link', 'image']
        ]
    };

    configCustom = {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
            ['blockquote', 'code-block'],
            [{'list': 'ordered'}, {'list': 'bullet'}],
            [{'script': 'sub'}, {'script': 'super'}],
            [{'align': []}],
            ['clean'],
            ['link', 'image']
        ]
    };


    constructor(public fb: FormBuilder) {
        this.validationForm = fb.group({
            emailFormEx: [null, [Validators.required, Validators.email]],
            passwordFormEx: [null, Validators.required],
        });
    }

    ngOnInit() {
        this.person = new PersonInfo();
        this.languages.unshift(new Language());
        this.experiences.unshift(new Experience());
        this.education.unshift(new Education());
        this.skills.unshift(new Skill());

        this.editorForm = new FormGroup({
            'editor': new FormControl(null)
        })
    }


    get emailFormEx() {
        return this.validationForm.get('emailFormEx');
    }

    onSelectFile(event: any) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();

            reader.readAsDataURL(event.target.files[0]); // read file as data url

            reader.onload = (event) => { // called once readAsDataURL is completed
                this.personPhoto = event.target['result'];
                console.log(this.personPhoto);
            }
        }
    }

    addLanguage() {
        this.languages.unshift(new Language());
    }

    removeLanguage(index) {
        this.languages.splice(index, 1);
    }

    addExperience() {
        this.experiences.unshift(new Experience());
    }

    removeExperience(index) {
        this.experiences.splice(index, 1);
    }

    addEducation() {
        this.education.unshift(new Education());
    }

    removeEducation(index) {
        this.education.splice(index, 1);
    }

    addSkill() {
        this.skills.unshift(new Skill())
    }

    removeSkill(index) {
        this.skills.splice(index, 1);
    }

}


