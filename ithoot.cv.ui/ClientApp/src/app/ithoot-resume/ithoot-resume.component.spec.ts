import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IthootResumeComponent } from './ithoot-resume.component';

describe('IthootResumeComponent', () => {
  let component: IthootResumeComponent;
  let fixture: ComponentFixture<IthootResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IthootResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IthootResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
