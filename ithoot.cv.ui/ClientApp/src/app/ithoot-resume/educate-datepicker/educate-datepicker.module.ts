import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EducateDatepickerComponent} from './educate-datepicker.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InputsModule, InputUtilitiesModule} from 'angular-bootstrap-md';
import {MatDatepickerModule, MatInputModule, MatNativeDateModule} from '@angular/material';


@NgModule({
    declarations: [
        EducateDatepickerComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        InputsModule,
        InputUtilitiesModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule
    ],
    exports: [
        EducateDatepickerComponent
    ]
})
export class EducateDatepickerModule {
}
