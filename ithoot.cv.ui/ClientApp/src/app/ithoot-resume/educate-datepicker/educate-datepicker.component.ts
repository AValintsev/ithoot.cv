import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MatDatepicker} from '@angular/material/datepicker';
import * as _moment from 'moment';
// @ts-ignore
import {default as _rollupMoment, Moment} from 'moment';
import {Experience} from '../../../code/models/person-info';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-educate-datepicker',
  templateUrl: './educate-datepicker.component.html',
  styleUrls: ['./educate-datepicker.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})

export class EducateDatepickerComponent implements OnInit {

  @Input('educationData') startEducateDate: Experience;
  @Input('educationDataEnd') educateEndDate: Experience;


  date = new FormControl(moment());
  dateEnd = new FormControl(moment());

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>, startDate: Moment) {
    const startDateValue = this.date.value;
    startDateValue.year(normalizedYear.year());
    this.date.setValue(startDateValue);
    datepicker.close();
  }

  chosenEndYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>, endDate: Moment) {
    const endDateValue = this.dateEnd.value;
    endDateValue.year(normalizedYear.year());
    this.dateEnd.setValue(endDateValue);
    datepicker.close();
  }

  constructor() { }

  ngOnInit() {
  }

}
