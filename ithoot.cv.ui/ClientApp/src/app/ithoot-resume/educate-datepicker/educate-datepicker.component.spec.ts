import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducateDatepickerComponent } from './educate-datepicker.component';

describe('EducateDatepickerComponent', () => {
  let component: EducateDatepickerComponent;
  let fixture: ComponentFixture<EducateDatepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducateDatepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducateDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
