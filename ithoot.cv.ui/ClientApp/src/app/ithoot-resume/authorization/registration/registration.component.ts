import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./../authorization.styles.scss']
})
export class RegistrationComponent implements OnInit {

    invalidRegister: boolean;
    isLoading = false;
    showErrors = false;
    successRegister: boolean;

    constructor(private http: HttpClient, private router: Router) {}

    ngOnInit() { }

    register(form: NgForm) {
        if (!this.validateForm(form)) {
            this.showErrors = true;
            return;
        }

        this.invalidRegister = false;
        this.isLoading = true;
        let credentials = JSON.stringify(form.value);
        this.http.post("https://localhost:44329/users-api/account/register", credentials, {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            })
        }).subscribe(response => {
            this.invalidRegister = false;
            this.successRegister = true;
            this.isLoading = false;
        }, err => {
                this.invalidRegister = true;
                this.successRegister = false;
                this.isLoading = false;
        });
    }

    validateForm(form: NgForm): boolean {
        return form.valid && form.controls['password'].value == form.controls['confirmPassword'].value;
    }
}
