/*
    The Error Interceptor перехватывает HTTP-ответы от API, чтобы проверить наличие ошибок.  Если имеется 401
 Несанкционированный ответ, пользователь автоматически выходит из приложения, все остальные ошибки повторно
  вызываются в вызывающую службу, поэтому пользователю может отображаться предупреждающее сообщение об ошибке.
     Он реализован с использованием класса HttpInterceptor, который был представлен в Angular 4.3 как часть нового модуля HttpClientModule.  Расширяя класс HttpInterceptor, вы можете создать собственный перехватчик, чтобы перехватывать все ответы об ошибках с сервера в одном месте.
    Перехватчики Http добавляются в конвейер запросов в разделе провайдеров файла authorization.module.ts
 */


import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthenticationService} from "../../../../code/services";


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if ([401, 403].indexOf(err.status) !== -1) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                this.authenticationService.logout();
                location.reload(true);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}
