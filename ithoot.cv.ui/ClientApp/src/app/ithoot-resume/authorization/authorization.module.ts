import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {AdminComponent} from './admin/admin.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ErrorInterceptor, fakeBackendProvider, JwtInterceptor} from "./_helpers";
import {RouterModule} from "@angular/router";
import {ButtonsModule, InputsModule, WavesModule} from 'angular-bootstrap-md';
import { RegistrationComponent } from './registration/registration.component'
import { AlertService } from "../../../code/services";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
    declarations: [LoginComponent, AdminComponent, RegistrationComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        FormsModule,
        InputsModule,
        WavesModule,
        ButtonsModule,
        FontAwesomeModule
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},

        // provider used to create fake backend
        fakeBackendProvider,
        AlertService
    ],
    exports: [LoginComponent, AdminComponent, RegistrationComponent]
})
export class AuthorizationModule {
}
