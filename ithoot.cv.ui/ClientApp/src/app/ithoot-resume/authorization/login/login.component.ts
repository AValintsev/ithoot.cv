import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../../../code/models';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./../authorization.styles.scss']
})

export class LoginComponent implements OnInit {

    private userId: string;

    invalidLogin: boolean;
    isLoading = false;
    showErrors = false;

    constructor(
        private http: HttpClient,
        private router: Router,
        private route: ActivatedRoute) {

        this.route.queryParams.subscribe(params => {
            this.userId = params['userId'];
        });
    }

    ngOnInit() {
        if (this.userId) {
            this.isLoading = true;

            this.confirmEmail(this.userId).subscribe(response => {
                alert("Email confirmed.");
                this.isLoading = false;
            }, error => {
                    alert("Some error occured during email confirmation.");
                    this.isLoading = false;
            });
        }
    }

    logIn(form: NgForm) {
        if (!this.validateForm(form)) {
            this.showErrors = true;
            return;
        }

        localStorage.removeItem("currentUser");
            
        this.invalidLogin = false;
        this.isLoading = true;
        let credentials = JSON.stringify(form.value);
        this.http.post("https://localhost:44329/users-api/account/login", credentials, {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            })
        }).subscribe(response => {
            let token = (<any>response).token;
            localStorage.setItem("jwt", token);
            localStorage.setItem("currentUser", form.controls['userName'].value);
            this.invalidLogin = false;
            this.isLoading = false;
            this.router.navigate(['/']);
        }, err => {
            this.invalidLogin = true;
            this.isLoading = false;
        });
    }

    validateForm(form: NgForm): boolean {
        return form.valid;
    }

    private confirmEmail(userId: string) {
        return this.http.get<any>(`https://localhost:44329/users-api/account/confirm?userId=${userId}`);
    }
}
