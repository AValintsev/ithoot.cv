import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LayoutsComponent} from './layouts.component';
import {RouterModule} from '@angular/router';
import {ButtonsModule, CardsFreeModule, WavesModule} from 'angular-bootstrap-md'


@NgModule({
  declarations: [
      LayoutsComponent
  ],
  imports: [
    CommonModule,
      RouterModule.forChild([{
        path:'', component:LayoutsComponent
      }]),
    ButtonsModule,
    CardsFreeModule,
    WavesModule
  ]
})
export class LayoutsModule { }
