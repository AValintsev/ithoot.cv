import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Experience} from '../../../code/models/person-info';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MatDatepicker} from '@angular/material/datepicker';

// @ts-ignore
import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
    parse: {
        dateInput: 'MMM-YYYY',
    },
    display: {
        dateInput: 'MMM-YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@Component({
    selector: 'app-datepicker',
    templateUrl: './datepicker.component.html',
    styleUrls: ['./datepicker.component.scss'],
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    ]
})

export class DatepickerComponent implements OnInit {

    @Input('data') startDate: Experience;
    @Input('dataEnd') endDate: Experience;


    date = new FormControl(moment());
    dateEnd = new FormControl(moment());

    chosenYearHandler(normalizedYear: Moment) {
        const startDateValue = this.date.value;
        startDateValue.year(normalizedYear.year());
        this.date.setValue(startDateValue);
    }

    chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>, startDate: Moment) {
        const startDateValue = this.date.value;
        startDateValue.month(normalizedMonth.month());
        this.date.setValue(startDateValue);
        datepicker.close();
    }

    chosenEndYearHandler(normalizedYear: Moment) {
        const endDateValue = this.dateEnd.value;
        endDateValue.year(normalizedYear.year());
        this.dateEnd.setValue(endDateValue);
    }


    chosenEndMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>, endDate: Moment) {
        const endDateValue = this.dateEnd.value;
        endDateValue.month(normalizedMonth.month());
        this.dateEnd.setValue(endDateValue);
        datepicker.close();
    }

    constructor() {
    }

    ngOnInit() {
    }
}
