import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DatepickerComponent} from './datepicker.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InputsModule, InputUtilitiesModule} from 'angular-bootstrap-md';
import {MatDatepickerModule, MatInputModule, MatNativeDateModule} from '@angular/material';

@NgModule({
    declarations: [
        DatepickerComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        InputsModule,
        InputUtilitiesModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule
    ],
    exports: [
        DatepickerComponent
    ]
})
export class DatepickerModule {
}
