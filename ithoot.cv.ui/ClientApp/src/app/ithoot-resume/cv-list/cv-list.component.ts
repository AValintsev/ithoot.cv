import {Component, OnInit} from '@angular/core';
import {CurriculumService} from '../../../code/services/curriculum.service';
import {PersonInfo} from '../../../code/models/person-info';


@Component({
    selector: 'app-cv-list',
    templateUrl: './cv-list.component.html',
    styleUrls: ['./cv-list.component.scss']
})

export class CvListComponent implements OnInit {
    cvs: Array<PersonInfo> = [];

    constructor(
        private cvService: CurriculumService
    ) {
    }

    ngOnInit() {
        this.cvs = this.cvService.getPersons();
    }
}
