import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CvListComponent} from "./cv-list.component";
import {RouterModule} from "@angular/router";
import {ButtonsModule, CardsFreeModule, WavesModule} from 'angular-bootstrap-md'
import {CvItemModule} from "../cv-item/cv-item.module";

@NgModule({
    declarations: [
        CvListComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([{
            path: '', component: CvListComponent
        }]),
        ButtonsModule,
        WavesModule,
        CardsFreeModule,
        CvItemModule,

    ]
})
export class CvListModule {
}
