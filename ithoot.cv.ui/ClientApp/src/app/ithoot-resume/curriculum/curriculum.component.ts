import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CurriculumService} from "../../../code/services/curriculum.service";
import {PersonInfo} from "../../../code/models/person-info";
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import * as SPINNER from 'ngx-ui-loader';
import {NgxUiLoaderService} from 'ngx-ui-loader';


@Component({
    selector: 'app-curriculum',
    templateUrl: './curriculum.component.html',
    styleUrls: ['./curriculum.component.scss']
})
export class CurriculumComponent implements OnInit {
    @ViewChild('contentToConvert') content: ElementRef;
    public persons: Array<PersonInfo> = [];
    public person: PersonInfo;
    public languages;
    public experiences;
    public education;
    public skills;
    public spinner = SPINNER;

    constructor(
        private curriculumService: CurriculumService,
        private ngxService: NgxUiLoaderService
    ) {
    }

    ngOnInit() {
        this.persons = this.curriculumService.getPersons();
        this.person = this.persons[0];
        this.languages = this.persons[0].languages;
        this.experiences = this.persons[0].experiences;
        this.education = this.persons[0].education;
        this.skills = this.persons[0].skills;

    }


    public exportToPdf() {
        this.ngxService.start();
        var data = document.getElementById('contentToConvert');
        html2canvas(data, {
            logging: false,
            useCORS: true,
            scale: 0.8,
        }).then(canvas => {
            //! MAKE YOUR PDF
            var pdf = new jsPDF('p', 'pt', 'letter');

            for (var i = 0; i <= (data.clientHeight / 1.32814) / 980 - 1; i++) {
                //! This is all just html2canvas stuff
                var srcImg = canvas;
                var sX = 0;
                var sY = 1293 * i; // start 980 pixels down for every new page
                var sWidth = 1440;
                var sHeight = 2200;
                var dX = 0;
                var dY = 0;
                var dWidth = 1400;
                var dHeight = 1780;

                var onePageCanvas = document.createElement("canvas");

                onePageCanvas.setAttribute('width', '1440');
                onePageCanvas.setAttribute('height', '2180');
                var ctx = onePageCanvas.getContext('2d');
                ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);
                var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

                var width = onePageCanvas.width;
                var height = onePageCanvas.clientHeight;


                if (i > 0) {
                    pdf.addPage(612, 791); //8.5" x 11" in pts (in*72)
                }

                pdf.setPage(i + 1);
                pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width * .68), (height * .61));

            }
            //! after the for loop is finished running, we save the pdf.
            pdf.save('testPDF.pdf');
        }).finally(() => {
            this.ngxService.stop();
        });
    };


}
