import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {CurriculumComponent} from './curriculum.component';
import {ButtonsModule, CollapseModule, IconsModule, WavesModule} from 'angular-bootstrap-md';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSliderModule} from '@angular/material';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {library} from '@fortawesome/fontawesome-svg-core';
import {faFileExport, fas} from "@fortawesome/free-solid-svg-icons";
import {NgxUiLoaderModule} from "ngx-ui-loader";

library.add(faFileExport, fas);

@NgModule({
    declarations: [
        CurriculumComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([{
            path: '', component: CurriculumComponent
        }]),
        IconsModule,
        MatProgressBarModule,
        MatSliderModule,
        ButtonsModule,
        WavesModule,
        CollapseModule,
        FontAwesomeModule,
        NgxUiLoaderModule
    ]
})
export class CurriculumModule {
}
