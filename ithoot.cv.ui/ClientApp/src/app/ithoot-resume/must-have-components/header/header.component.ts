import { Component, OnInit } from '@angular/core';
import { navItems} from './nav-items';
import {User} from "../../../../code/models";
import {AuthenticationService, UserService} from "../../../../code/services";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: string;
  userFromApi: User;
  navItems = navItems;
  constructor(
      private userService: UserService,
      private authenticationService: AuthenticationService,
      private router: Router
  ) {
      //this.currentUser = this.authenticationService.currentUserValue;
      this.currentUser = localStorage.getItem("currentUser");
  }

  ngOnInit() {
    /*this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
      this.userFromApi = user;
    });*/
  }

  logout() {
      //this.authenticationService.logout();
      localStorage.removeItem("jwt");
      localStorage.removeItem("currentUser");
    this.router.navigate(['/login'])
  }

}
