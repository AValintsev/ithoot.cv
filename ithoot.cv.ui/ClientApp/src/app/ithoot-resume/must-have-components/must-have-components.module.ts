import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from "./header/header.component";
import {FooterComponent} from "./footer/footer.component";
import {RouterModule} from "@angular/router";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
// For MDB Angular Free
import {NavbarModule, WavesModule, ButtonsModule, IconsModule, DropdownModule} from 'angular-bootstrap-md';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCoffee, faHome, faEnvelope, faUser, faFile, faAddressCard, faClone } from '@fortawesome/free-solid-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';



library.add(faCoffee, faHome, faEnvelope, faUser, faFile, faAddressCard, faClone);
library.add(fas, far);



@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        BrowserAnimationsModule,
        NavbarModule,
        WavesModule,
        ButtonsModule,
        IconsModule,
        DropdownModule,
        FontAwesomeModule
    ],
    exports: [
        HeaderComponent,
        FooterComponent
    ]
})
export class MustHaveComponentsModule {
}
