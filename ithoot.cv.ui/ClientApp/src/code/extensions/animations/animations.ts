import { trigger, transition, query, style, group, animate, animateChild } from '@angular/animations';

export const slideInAnimation =
    trigger('routeAnimations', [
        // route 'enter' transition
        transition('* <=> *', [

            // styles at start of transition
            style({ opacity: 0 }),

            // animation and styles at end of transition
            animate('.40s', style({ opacity: 1 }))
        ]),
    ]);
