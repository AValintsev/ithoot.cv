import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {User} from "../models";

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        // return this.http.get<User[]>(`${config.apiUrl}/users`); with ${config.apiUrl} don't work!!!!!
        return this.http.get<User[]>(`/users`);
    }

    getById(id: number) {
        // return this.http.get<User>(`${config.apiUrl}/users/${id}`); don't work!!!!!
        return this.http.get<User>(`/users/${id}`);
    }
    register(user: User) {
        return this.http.post(`${config.apiUrl}/users/register`, user);
    }
    update(user: User) {
        return this.http.put(`${config.apiUrl}/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`${config.apiUrl}/users/` + id);
    }
}
