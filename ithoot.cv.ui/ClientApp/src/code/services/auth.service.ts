import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModel } from '../models/login.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    private baseUrl = 'https://localhost:44388/api/account';

    constructor(private http: HttpClient) { }

    login(model: UserModel): Observable<object> {
        return this.http.post(`${this.baseUrl}`, model);
    }
}
