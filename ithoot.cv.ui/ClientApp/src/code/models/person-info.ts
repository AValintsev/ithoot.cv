import {SafeHtml} from "@angular/platform-browser";
import {Moment} from "moment";
import {FormControl} from "@angular/forms";

export class PersonInfo {
        public id: number;
        public photo: string;
        public name: string;
        public position: string;
        public town: string;
        public country: string;
        public languages: Array<Language>;
        public mail: string;
        public street: string;
        public about: string;

        public  experiences: Array<Experience>;

        public education: Array<Education>;

        public skills: Array<Skill>;
}

export  class Language {
        public language: string;
        public level: string
}


export class Experience {
        public startDate: Moment;
        public endDate: Moment;
        public position: string;
        public company: string;
        public info: SafeHtml;

}

export class Education {
        public startDate: Moment;
        public endDate: Moment;
        public university: string;
        public degree: string;

}

export  class Skill {
        public name: string;
        public level: number;
}
