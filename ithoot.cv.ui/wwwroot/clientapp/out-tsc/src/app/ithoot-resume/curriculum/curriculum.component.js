var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { CurriculumService } from "../../../code/services/curriculum.service";
var CurriculumComponent = /** @class */ (function () {
    function CurriculumComponent(curriculumService) {
        this.curriculumService = curriculumService;
        this.persons = [];
    }
    CurriculumComponent.prototype.ngOnInit = function () {
        this.persons = this.curriculumService.getPersons();
        this.person = this.persons[0];
        this.languages = this.persons[0].languages;
        this.experiences = this.persons[0].experiences;
        this.education = this.persons[0].education;
        this.skills = this.persons[0].skills;
    };
    CurriculumComponent = __decorate([
        Component({
            selector: 'app-curriculum',
            templateUrl: './curriculum.component.html',
            styleUrls: ['./curriculum.component.scss']
        }),
        __metadata("design:paramtypes", [CurriculumService])
    ], CurriculumComponent);
    return CurriculumComponent;
}());
export { CurriculumComponent };
//# sourceMappingURL=curriculum.component.js.map