var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { CurriculumComponent } from "./curriculum.component";
import { IconsModule } from "angular-bootstrap-md";
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSliderModule } from "@angular/material";
var CurriculumModule = /** @class */ (function () {
    function CurriculumModule() {
    }
    CurriculumModule = __decorate([
        NgModule({
            declarations: [
                CurriculumComponent
            ],
            imports: [
                CommonModule,
                RouterModule.forChild([{
                        path: '', component: CurriculumComponent
                    }]),
                IconsModule,
                MatProgressBarModule,
                MatSliderModule
            ]
        })
    ], CurriculumModule);
    return CurriculumModule;
}());
export { CurriculumModule };
//# sourceMappingURL=curriculum.module.js.map