var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
// For MDB Angular Free
import { NavbarModule, WavesModule, ButtonsModule, IconsModule, DropdownModule } from 'angular-bootstrap-md';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCoffee, faHome, faEnvelope, faUser, faFile, faAddressCard, faClone } from '@fortawesome/free-solid-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
library.add(faCoffee, faHome, faEnvelope, faUser, faFile, faAddressCard, faClone);
library.add(fas, far);
var MustHaveComponentsModule = /** @class */ (function () {
    function MustHaveComponentsModule() {
    }
    MustHaveComponentsModule = __decorate([
        NgModule({
            declarations: [
                HeaderComponent,
                FooterComponent
            ],
            imports: [
                CommonModule,
                RouterModule,
                BrowserAnimationsModule,
                NavbarModule,
                WavesModule,
                ButtonsModule,
                IconsModule,
                DropdownModule,
                FontAwesomeModule
            ],
            exports: [
                HeaderComponent,
                FooterComponent
            ]
        })
    ], MustHaveComponentsModule);
    return MustHaveComponentsModule;
}());
export { MustHaveComponentsModule };
//# sourceMappingURL=must-have-components.module.js.map