export var navItems = [
    {
        title: 'Layouts',
        icon: 'clone',
        isActive: 'active',
        link: '/layouts'
    },
    {
        title: 'My resume',
        icon: 'address-card',
        isActive: false,
        link: '/cv-list'
    },
];
//# sourceMappingURL=nav-items.js.map