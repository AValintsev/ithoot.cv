var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IthootResumeComponent } from './ithoot-resume/ithoot-resume.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MustHaveComponentsModule } from "./ithoot-resume/must-have-components";
import { MatProgressBarModule, MatSliderModule } from "@angular/material";
import { QuillModule } from "ngx-quill";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                IthootResumeComponent
            ],
            imports: [
                BrowserModule,
                AppRoutingModule,
                MDBBootstrapModule.forRoot(),
                BrowserAnimationsModule,
                MatProgressBarModule,
                MatSliderModule,
                MustHaveComponentsModule,
                ReactiveFormsModule,
                QuillModule,
                FormsModule,
                NgbModule
            ],
            providers: [],
            bootstrap: [AppComponent],
            schemas: [NO_ERRORS_SCHEMA]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map