var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IthootResumeComponent } from './ithoot-resume/ithoot-resume.component';
var routes = [
    {
        path: '',
        component: IthootResumeComponent,
        children: [
            {
                path: '',
                loadChildren: './ithoot-resume/about/about.module#AboutModule'
            },
            {
                path: 'curriculum',
                loadChildren: './ithoot-resume/curriculum/curriculum.module#CurriculumModule'
            },
            {
                path: 'cv-editor',
                loadChildren: './ithoot-resume/cv-editor/cv-editor.module#CvEditorModule'
            },
            {
                path: 'layouts',
                loadChildren: './ithoot-resume/layouts/layouts.module#LayoutsModule'
            },
            {
                path: 'cv-list',
                loadChildren: './ithoot-resume/cv-list/cv-list.module#CvListModule'
            },
            {
                path: 'cl-editor',
                loadChildren: './ithoot-resume/cl-editor/cl-editor.module#ClEditorModule'
            },
            {
                path: 'cv-new',
                loadChildren: './ithoot-resume/cv-new/cv-new.module#CvNewModule'
            }
        ]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map