import { trigger, transition, style, animate } from '@angular/animations';
export var slideInAnimation = trigger('routeAnimations', [
    // route 'enter' transition
    transition('* <=> *', [
        // styles at start of transition
        style({ opacity: 0 }),
        // animation and styles at end of transition
        animate('.40s', style({ opacity: 1 }))
    ]),
]);
//# sourceMappingURL=animations.js.map