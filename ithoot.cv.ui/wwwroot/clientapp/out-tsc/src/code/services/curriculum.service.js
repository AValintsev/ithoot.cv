var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import * as moment from 'moment';
var CurriculumService = /** @class */ (function () {
    function CurriculumService(sanitizer) {
        this.sanitizer = sanitizer;
        this.persons = [
            {
                id: 1,
                photo: 'Anton.jpg',
                name: 'ANTON',
                position: 'Full-stack Senior ASP.NET Developer',
                town: 'Khmelnitsky',
                country: 'Ukraine',
                languages: [
                    {
                        language: 'English',
                        level: 'Intermediate'
                    },
                    {
                        language: 'Ukraine',
                        level: 'Native'
                    },
                    {
                        language: 'Russian',
                        level: 'Native'
                    }
                ],
                mail: 'mail@gmail.com',
                street: 'Zarychanska, 14/5',
                about: 'I’m Full-stack ASP.NET developer with 10+ years of experience in building web apps using' +
                    ' Microsoft technologies like C#, ASP.NET MVC, Visual Studio, MS SQL Server and others. For building Backend I use ASP.NET MVC, MS SQL Server, Web API, WCF, Entity Framework For Frontend I use HTML, CSS, JavaScript, Ajax, jQuery, Bootstrap, Knockout, Angular etc.I\'ve started as junior developer and now I\'m TeamLead/Project Manager',
                experiences: [
                    {
                        startDate: moment('2016-01'),
                        endDate: moment('2016-07'),
                        position: 'Senior Developer',
                        company: 'UIB First Ukrainian International Bank',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>Very interested experience. I first saw the solution which contains more than 700 projects!</p>\n                  <p>Here I worked with <a href=\"#\">Oracle</a>, <a href=\"#\">UML</a> modeling and <a href=\"#\">MSWorkflows</a>.</p>\n                  <p>Used technologies: ASP.NET MVC, C#,  Web API, WCF, JavaScript, MS SQL Server 2012, Oracle data base, Visual Studio 2015, Entity Framework, UML, Ms Workflows</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used JIRA for time planning, tasks management and code review.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2016-03'),
                        endDate: moment('2015-02'),
                        position: 'Senior Developer, TechLead',
                        company: 'Infopulse. Big Four company',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>I've started working in Infopulse in new role for me is a Senior developer. I estimated tasks, reviewed code and helped less experienced collegues. We developed internal projects for one of Big Four company.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Portal for management mandatory trainings and courses</a> for company's employees worldwide. Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Unit testing, Knockout, jQuery, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Admin tool</a> for existing project which allows to change html templates stored in DB, edit labels, texts, rules etc. Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL, Server 2008, Visual Studio 2012, Entity Framework, Unit testing, jQuery, Angular, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">The Audit Quality Monitoring (AQM)</a> process supports company's Audit teams by establishing a standard and consistent way to track progress towards and completion of engagement-related milestones. AQM provides Audit leadership with information it needs to support the completion of each milestone at the national, regional, office, and individual AQM profile [engagement] levels.</p>\n                  <p>Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Entity Framework, Unit testing, jQuery, Angular, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>In last project, my role was TechLead. I worked closely with architects to build the system correctly, I estimated not only tasks but sprints and projects, I managed a part of big team. Team was big and international. Top management in USA, testing team in India and Dev team in Ukraine. 10 developers were in my jurisdiction.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used TFS for time planning and tasks management.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2003-09'),
                        endDate: moment('2013-03'),
                        position: 'Developer',
                        company: 'Ciklum. LanguageWire Agito Translate',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>Ciklum, and LanguageWire project which specialization is translation service was my longest place I've worked. I participated in 3 projects simultaneously.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Agito Translate</a> - tool which provides ability to upload documents in different formats (*.doc, *.xls etc) to the system and translate them. There was ability to pretranlsate whole document and use pretranslated text from knowledge base or input text manually. Used  technologies: ASP.NET WebForms, C#, WCF, Ajax, jQuery, MS SQL Server 2008, Visual Studio 2008.</p>\n                  <p>Third part component: MemoQ.</p>\n                  <p>Project almost didn't use post backs based on forms, it used an ajax requests. That was SPA design.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">WeAllEdit</a> tool which allows to edit InDesign (*.indd) documents. Main feature is ability to upload *.indd documents, edit them and then download them back in different formats: *.indd, *.pdf, *.html.</p>\n                  <p>Used technologies:  ASP.NET WebForms, C#, WCF, Ajax, jQuery, MS SQL Server 2008, Visual Studio 2012.</p>\n                  <p>Third part components: InDesign servers version from 3 to 6 which provided theirs com objects. These servers had hang time to time and distributing approach were developed allows to choose server which is working.</p>\n                  <p>Project used post backs based on forms in part where documents uploaded/downloaded and SPA design in editor.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">BMS 2.0</a> - CRM system which was rebuilt from classic ASP to ASP.NET MVC.</p>\n                  <p>Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Entity Framework, Unit testing, LESS, Bootstrap.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used TFS for time planning and tasks management.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2007-07'),
                        endDate: moment('2008-09'),
                        position: 'Junior Developer',
                        company: 'Krendls',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>That was my first job. Here I started working with C#, Visual Studio 2008, MS SQL Server. I used ASP.NET Web Forms, User Controls,Ajax, AjaxToolkit, WebParts, WebServices (*.smx). I became closer with HMTL and CSS. And we used DotNetNuke cms for one of the project. Team used JIRA for tasks management.</p>\n                </div>")
                    },
                ],
                education: [
                    {
                        startDate: moment('2002'),
                        endDate: moment('2007'),
                        university: 'Khmelnitsky National University',
                        degree: 'Information Technologies of Design'
                    }
                ],
                skills: [
                    {
                        name: 'C#',
                        level: 7,
                    },
                    {
                        name: 'ASP.NET MVC',
                        level: 6,
                    },
                    {
                        name: 'Visual Studio',
                        level: 8,
                    },
                    {
                        name: 'LINQ',
                        level: 5,
                    },
                    {
                        name: 'Entity Framework',
                        level: 4,
                    },
                    {
                        name: 'Web API',
                        level: 3,
                    },
                    {
                        name: 'WCF',
                        level: 2,
                    },
                    {
                        name: 'SQL',
                        level: 7,
                    },
                    {
                        name: 'MS SQL Server',
                        level: 9,
                    },
                    {
                        name: 'MySql',
                        level: 5,
                    },
                    {
                        name: 'Ajax',
                        level: 4,
                    },
                    {
                        name: 'jQuery',
                        level: 3,
                    },
                    {
                        name: 'HTML',
                        level: 2,
                    },
                    {
                        name: 'CSS',
                        level: 1,
                    },
                    {
                        name: 'Bootstrap',
                        level: 8,
                    },
                    {
                        name: 'Materialize',
                        level: 7,
                    },
                    {
                        name: 'Less',
                        level: 5,
                    },
                    {
                        name: 'TFS',
                        level: 3,
                    },
                    {
                        name: 'SVN',
                        level: 1,
                    },
                    {
                        name: 'Git',
                        level: 7,
                    },
                    {
                        name: 'Jira',
                        level: 5,
                    },
                    {
                        name: 'Angular',
                        level: 6,
                    },
                    {
                        name: 'Knockout',
                        level: 4,
                    },
                    {
                        name: 'JavaScript',
                        level: 3,
                    },
                ]
            },
            {
                id: 2,
                photo: 'Anton.jpg',
                name: 'ANTON',
                position: 'Full-stack Senior ASP.NET Developer',
                town: 'Khmelnitsky',
                country: 'Ukraine',
                languages: [
                    {
                        language: 'English',
                        level: 'Intermediate'
                    },
                    {
                        language: 'Ukraine',
                        level: 'Native'
                    },
                    {
                        language: 'Russian',
                        level: 'Native'
                    }
                ],
                mail: 'mail@gmail.com',
                street: 'Zarychanska, 14/5',
                about: 'I’m Full-stack ASP.NET developer with 10+ years of experience in building web apps using' +
                    ' Microsoft technologies like C#, ASP.NET MVC, Visual Studio, MS SQL Server and others. For building Backend I use ASP.NET MVC, MS SQL Server, Web API, WCF, Entity Framework For Frontend I use HTML, CSS, JavaScript, Ajax, jQuery, Bootstrap, Knockout, Angular etc.I\'ve started as junior developer and now I\'m TeamLead/Project Manager',
                experiences: [
                    {
                        startDate: moment('2016-01'),
                        endDate: moment('2016-07'),
                        position: 'Senior Developer',
                        company: 'UIB First Ukrainian International Bank',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>Very interested experience. I first saw the solution which contains more than 700 projects!</p>\n                  <p>Here I worked with <a href=\"#\">Oracle</a>, <a href=\"#\">UML</a> modeling and <a href=\"#\">MSWorkflows</a>.</p>\n                  <p>Used technologies: ASP.NET MVC, C#,  Web API, WCF, JavaScript, MS SQL Server 2012, Oracle data base, Visual Studio 2015, Entity Framework, UML, Ms Workflows</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used JIRA for time planning, tasks management and code review.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2016-03'),
                        endDate: moment('2015-02'),
                        position: 'Senior Developer, TechLead',
                        company: 'Infopulse. Big Four company',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>I've started working in Infopulse in new role for me is a Senior developer. I estimated tasks, reviewed code and helped less experienced collegues. We developed internal projects for one of Big Four company.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Portal for management mandatory trainings and courses</a> for company's employees worldwide. Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Unit testing, Knockout, jQuery, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Admin tool</a> for existing project which allows to change html templates stored in DB, edit labels, texts, rules etc. Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL, Server 2008, Visual Studio 2012, Entity Framework, Unit testing, jQuery, Angular, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">The Audit Quality Monitoring (AQM)</a> process supports company's Audit teams by establishing a standard and consistent way to track progress towards and completion of engagement-related milestones. AQM provides Audit leadership with information it needs to support the completion of each milestone at the national, regional, office, and individual AQM profile [engagement] levels.</p>\n                  <p>Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Entity Framework, Unit testing, jQuery, Angular, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>In last project, my role was TechLead. I worked closely with architects to build the system correctly, I estimated not only tasks but sprints and projects, I managed a part of big team. Team was big and international. Top management in USA, testing team in India and Dev team in Ukraine. 10 developers were in my jurisdiction.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used TFS for time planning and tasks management.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2003-09'),
                        endDate: moment('2013-03'),
                        position: 'Developer',
                        company: 'Ciklum. LanguageWire Agito Translate',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>Ciklum, and LanguageWire project which specialization is translation service was my longest place I've worked. I participated in 3 projects simultaneously.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Agito Translate</a> - tool which provides ability to upload documents in different formats (*.doc, *.xls etc) to the system and translate them. There was ability to pretranlsate whole document and use pretranslated text from knowledge base or input text manually. Used  technologies: ASP.NET WebForms, C#, WCF, Ajax, jQuery, MS SQL Server 2008, Visual Studio 2008.</p>\n                  <p>Third part component: MemoQ.</p>\n                  <p>Project almost didn't use post backs based on forms, it used an ajax requests. That was SPA design.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">WeAllEdit</a> tool which allows to edit InDesign (*.indd) documents. Main feature is ability to upload *.indd documents, edit them and then download them back in different formats: *.indd, *.pdf, *.html.</p>\n                  <p>Used technologies:  ASP.NET WebForms, C#, WCF, Ajax, jQuery, MS SQL Server 2008, Visual Studio 2012.</p>\n                  <p>Third part components: InDesign servers version from 3 to 6 which provided theirs com objects. These servers had hang time to time and distributing approach were developed allows to choose server which is working.</p>\n                  <p>Project used post backs based on forms in part where documents uploaded/downloaded and SPA design in editor.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">BMS 2.0</a> - CRM system which was rebuilt from classic ASP to ASP.NET MVC.</p>\n                  <p>Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Entity Framework, Unit testing, LESS, Bootstrap.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used TFS for time planning and tasks management.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2007-07'),
                        endDate: moment('2008-09'),
                        position: 'Junior Developer',
                        company: 'Krendls',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>That was my first job. Here I started working with C#, Visual Studio 2008, MS SQL Server. I used ASP.NET Web Forms, User Controls,Ajax, AjaxToolkit, WebParts, WebServices (*.smx). I became closer with HMTL and CSS. And we used DotNetNuke cms for one of the project. Team used JIRA for tasks management.</p>\n                </div>")
                    },
                ],
                education: [
                    {
                        startDate: moment('2002'),
                        endDate: moment('2007'),
                        university: 'Khmelnitsky National University',
                        degree: 'Information Technologies of Design'
                    }
                ],
                skills: [
                    {
                        name: 'C#',
                        level: 7,
                    },
                    {
                        name: 'ASP.NET MVC',
                        level: 6,
                    },
                    {
                        name: 'Visual Studio',
                        level: 8,
                    },
                    {
                        name: 'LINQ',
                        level: 5,
                    },
                    {
                        name: 'Entity Framework',
                        level: 4,
                    },
                    {
                        name: 'Web API',
                        level: 3,
                    },
                    {
                        name: 'WCF',
                        level: 2,
                    },
                    {
                        name: 'SQL',
                        level: 7,
                    },
                    {
                        name: 'MS SQL Server',
                        level: 9,
                    },
                    {
                        name: 'MySql',
                        level: 5,
                    },
                    {
                        name: 'Ajax',
                        level: 4,
                    },
                    {
                        name: 'jQuery',
                        level: 3,
                    },
                    {
                        name: 'HTML',
                        level: 2,
                    },
                    {
                        name: 'CSS',
                        level: 1,
                    },
                    {
                        name: 'Bootstrap',
                        level: 8,
                    },
                    {
                        name: 'Materialize',
                        level: 7,
                    },
                    {
                        name: 'Less',
                        level: 5,
                    },
                    {
                        name: 'TFS',
                        level: 3,
                    },
                    {
                        name: 'SVN',
                        level: 1,
                    },
                    {
                        name: 'Git',
                        level: 7,
                    },
                    {
                        name: 'Jira',
                        level: 5,
                    },
                    {
                        name: 'Angular',
                        level: 6,
                    },
                    {
                        name: 'Knockout',
                        level: 4,
                    },
                    {
                        name: 'JavaScript',
                        level: 3,
                    },
                ]
            },
            {
                id: 3,
                photo: 'Anton.jpg',
                name: 'ANTON',
                position: 'Full-stack Senior ASP.NET Developer',
                town: 'Khmelnitsky',
                country: 'Ukraine',
                languages: [
                    {
                        language: 'English',
                        level: 'Intermediate'
                    },
                    {
                        language: 'Ukraine',
                        level: 'Native'
                    },
                    {
                        language: 'Russian',
                        level: 'Native'
                    }
                ],
                mail: 'mail@gmail.com',
                street: 'Zarychanska, 14/5',
                about: 'I’m Full-stack ASP.NET developer with 10+ years of experience in building web apps using' +
                    ' Microsoft technologies like C#, ASP.NET MVC, Visual Studio, MS SQL Server and others. For building Backend I use ASP.NET MVC, MS SQL Server, Web API, WCF, Entity Framework For Frontend I use HTML, CSS, JavaScript, Ajax, jQuery, Bootstrap, Knockout, Angular etc.I\'ve started as junior developer and now I\'m TeamLead/Project Manager',
                experiences: [
                    {
                        startDate: moment('2016-01'),
                        endDate: moment('2016-07'),
                        position: 'Senior Developer',
                        company: 'UIB First Ukrainian International Bank',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>Very interested experience. I first saw the solution which contains more than 700 projects!</p>\n                  <p>Here I worked with <a href=\"#\">Oracle</a>, <a href=\"#\">UML</a> modeling and <a href=\"#\">MSWorkflows</a>.</p>\n                  <p>Used technologies: ASP.NET MVC, C#,  Web API, WCF, JavaScript, MS SQL Server 2012, Oracle data base, Visual Studio 2015, Entity Framework, UML, Ms Workflows</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used JIRA for time planning, tasks management and code review.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2016-03'),
                        endDate: moment('2015-02'),
                        position: 'Senior Developer, TechLead',
                        company: 'Infopulse. Big Four company',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>I've started working in Infopulse in new role for me is a Senior developer. I estimated tasks, reviewed code and helped less experienced collegues. We developed internal projects for one of Big Four company.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Portal for management mandatory trainings and courses</a> for company's employees worldwide. Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Unit testing, Knockout, jQuery, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Admin tool</a> for existing project which allows to change html templates stored in DB, edit labels, texts, rules etc. Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL, Server 2008, Visual Studio 2012, Entity Framework, Unit testing, jQuery, Angular, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">The Audit Quality Monitoring (AQM)</a> process supports company's Audit teams by establishing a standard and consistent way to track progress towards and completion of engagement-related milestones. AQM provides Audit leadership with information it needs to support the completion of each milestone at the national, regional, office, and individual AQM profile [engagement] levels.</p>\n                  <p>Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Entity Framework, Unit testing, jQuery, Angular, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>In last project, my role was TechLead. I worked closely with architects to build the system correctly, I estimated not only tasks but sprints and projects, I managed a part of big team. Team was big and international. Top management in USA, testing team in India and Dev team in Ukraine. 10 developers were in my jurisdiction.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used TFS for time planning and tasks management.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2003-09'),
                        endDate: moment('2013-03'),
                        position: 'Developer',
                        company: 'Ciklum. LanguageWire Agito Translate',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>Ciklum, and LanguageWire project which specialization is translation service was my longest place I've worked. I participated in 3 projects simultaneously.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Agito Translate</a> - tool which provides ability to upload documents in different formats (*.doc, *.xls etc) to the system and translate them. There was ability to pretranlsate whole document and use pretranslated text from knowledge base or input text manually. Used  technologies: ASP.NET WebForms, C#, WCF, Ajax, jQuery, MS SQL Server 2008, Visual Studio 2008.</p>\n                  <p>Third part component: MemoQ.</p>\n                  <p>Project almost didn't use post backs based on forms, it used an ajax requests. That was SPA design.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">WeAllEdit</a> tool which allows to edit InDesign (*.indd) documents. Main feature is ability to upload *.indd documents, edit them and then download them back in different formats: *.indd, *.pdf, *.html.</p>\n                  <p>Used technologies:  ASP.NET WebForms, C#, WCF, Ajax, jQuery, MS SQL Server 2008, Visual Studio 2012.</p>\n                  <p>Third part components: InDesign servers version from 3 to 6 which provided theirs com objects. These servers had hang time to time and distributing approach were developed allows to choose server which is working.</p>\n                  <p>Project used post backs based on forms in part where documents uploaded/downloaded and SPA design in editor.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">BMS 2.0</a> - CRM system which was rebuilt from classic ASP to ASP.NET MVC.</p>\n                  <p>Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Entity Framework, Unit testing, LESS, Bootstrap.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used TFS for time planning and tasks management.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2007-07'),
                        endDate: moment('2008-09'),
                        position: 'Junior Developer',
                        company: 'Krendls',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>That was my first job. Here I started working with C#, Visual Studio 2008, MS SQL Server. I used ASP.NET Web Forms, User Controls,Ajax, AjaxToolkit, WebParts, WebServices (*.smx). I became closer with HMTL and CSS. And we used DotNetNuke cms for one of the project. Team used JIRA for tasks management.</p>\n                </div>")
                    },
                ],
                education: [
                    {
                        startDate: moment('2002'),
                        endDate: moment('2007'),
                        university: 'Khmelnitsky National University',
                        degree: 'Information Technologies of Design'
                    }
                ],
                skills: [
                    {
                        name: 'C#',
                        level: 7,
                    },
                    {
                        name: 'ASP.NET MVC',
                        level: 6,
                    },
                    {
                        name: 'Visual Studio',
                        level: 8,
                    },
                    {
                        name: 'LINQ',
                        level: 5,
                    },
                    {
                        name: 'Entity Framework',
                        level: 4,
                    },
                    {
                        name: 'Web API',
                        level: 3,
                    },
                    {
                        name: 'WCF',
                        level: 2,
                    },
                    {
                        name: 'SQL',
                        level: 7,
                    },
                    {
                        name: 'MS SQL Server',
                        level: 9,
                    },
                    {
                        name: 'MySql',
                        level: 5,
                    },
                    {
                        name: 'Ajax',
                        level: 4,
                    },
                    {
                        name: 'jQuery',
                        level: 3,
                    },
                    {
                        name: 'HTML',
                        level: 2,
                    },
                    {
                        name: 'CSS',
                        level: 1,
                    },
                    {
                        name: 'Bootstrap',
                        level: 8,
                    },
                    {
                        name: 'Materialize',
                        level: 7,
                    },
                    {
                        name: 'Less',
                        level: 5,
                    },
                    {
                        name: 'TFS',
                        level: 3,
                    },
                    {
                        name: 'SVN',
                        level: 1,
                    },
                    {
                        name: 'Git',
                        level: 7,
                    },
                    {
                        name: 'Jira',
                        level: 5,
                    },
                    {
                        name: 'Angular',
                        level: 6,
                    },
                    {
                        name: 'Knockout',
                        level: 4,
                    },
                    {
                        name: 'JavaScript',
                        level: 3,
                    },
                ]
            },
            {
                id: 4,
                photo: 'Anton.jpg',
                name: 'ANTON',
                position: 'Full-stack Senior ASP.NET Developer',
                town: 'Khmelnitsky',
                country: 'Ukraine',
                languages: [
                    {
                        language: 'English',
                        level: 'Intermediate'
                    },
                    {
                        language: 'Ukraine',
                        level: 'Native'
                    },
                    {
                        language: 'Russian',
                        level: 'Native'
                    }
                ],
                mail: 'mail@gmail.com',
                street: 'Zarychanska, 14/5',
                about: 'I’m Full-stack ASP.NET developer with 10+ years of experience in building web apps using' +
                    ' Microsoft technologies like C#, ASP.NET MVC, Visual Studio, MS SQL Server and others. For building Backend I use ASP.NET MVC, MS SQL Server, Web API, WCF, Entity Framework For Frontend I use HTML, CSS, JavaScript, Ajax, jQuery, Bootstrap, Knockout, Angular etc.I\'ve started as junior developer and now I\'m TeamLead/Project Manager',
                experiences: [
                    {
                        startDate: moment('2016-01'),
                        endDate: moment('2016-07'),
                        position: 'Senior Developer',
                        company: 'UIB First Ukrainian International Bank',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>Very interested experience. I first saw the solution which contains more than 700 projects!</p>\n                  <p>Here I worked with <a href=\"#\">Oracle</a>, <a href=\"#\">UML</a> modeling and <a href=\"#\">MSWorkflows</a>.</p>\n                  <p>Used technologies: ASP.NET MVC, C#,  Web API, WCF, JavaScript, MS SQL Server 2012, Oracle data base, Visual Studio 2015, Entity Framework, UML, Ms Workflows</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used JIRA for time planning, tasks management and code review.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2016-03'),
                        endDate: moment('2015-02'),
                        position: 'Senior Developer, TechLead',
                        company: 'Infopulse. Big Four company',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>I've started working in Infopulse in new role for me is a Senior developer. I estimated tasks, reviewed code and helped less experienced collegues. We developed internal projects for one of Big Four company.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Portal for management mandatory trainings and courses</a> for company's employees worldwide. Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Unit testing, Knockout, jQuery, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Admin tool</a> for existing project which allows to change html templates stored in DB, edit labels, texts, rules etc. Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL, Server 2008, Visual Studio 2012, Entity Framework, Unit testing, jQuery, Angular, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">The Audit Quality Monitoring (AQM)</a> process supports company's Audit teams by establishing a standard and consistent way to track progress towards and completion of engagement-related milestones. AQM provides Audit leadership with information it needs to support the completion of each milestone at the national, regional, office, and individual AQM profile [engagement] levels.</p>\n                  <p>Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Entity Framework, Unit testing, jQuery, Angular, LESS.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>In last project, my role was TechLead. I worked closely with architects to build the system correctly, I estimated not only tasks but sprints and projects, I managed a part of big team. Team was big and international. Top management in USA, testing team in India and Dev team in Ukraine. 10 developers were in my jurisdiction.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used TFS for time planning and tasks management.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2003-09'),
                        endDate: moment('2013-03'),
                        position: 'Developer',
                        company: 'Ciklum. LanguageWire Agito Translate',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>Ciklum, and LanguageWire project which specialization is translation service was my longest place I've worked. I participated in 3 projects simultaneously.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">Agito Translate</a> - tool which provides ability to upload documents in different formats (*.doc, *.xls etc) to the system and translate them. There was ability to pretranlsate whole document and use pretranslated text from knowledge base or input text manually. Used  technologies: ASP.NET WebForms, C#, WCF, Ajax, jQuery, MS SQL Server 2008, Visual Studio 2008.</p>\n                  <p>Third part component: MemoQ.</p>\n                  <p>Project almost didn't use post backs based on forms, it used an ajax requests. That was SPA design.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">WeAllEdit</a> tool which allows to edit InDesign (*.indd) documents. Main feature is ability to upload *.indd documents, edit them and then download them back in different formats: *.indd, *.pdf, *.html.</p>\n                  <p>Used technologies:  ASP.NET WebForms, C#, WCF, Ajax, jQuery, MS SQL Server 2008, Visual Studio 2012.</p>\n                  <p>Third part components: InDesign servers version from 3 to 6 which provided theirs com objects. These servers had hang time to time and distributing approach were developed allows to choose server which is working.</p>\n                  <p>Project used post backs based on forms in part where documents uploaded/downloaded and SPA design in editor.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p><a href=\"#\">BMS 2.0</a> - CRM system which was rebuilt from classic ASP to ASP.NET MVC.</p>\n                  <p>Used technologies: ASP.NET MVC, C#, Web API, jQuery, MS SQL Server 2008, Visual Studio 2012, Entity Framework, Unit testing, LESS, Bootstrap.</p>\n                </div>\n                <div class=\"main-paragraph\">\n                  <p>Team used TFS for time planning and tasks management.</p>\n                </div>")
                    },
                    {
                        startDate: moment('2007-07'),
                        endDate: moment('2008-09'),
                        position: 'Junior Developer',
                        company: 'Krendls',
                        info: this.sanitizer.bypassSecurityTrustHtml("<div class=\"first-paragraph\">\n                  <p>That was my first job. Here I started working with C#, Visual Studio 2008, MS SQL Server. I used ASP.NET Web Forms, User Controls,Ajax, AjaxToolkit, WebParts, WebServices (*.smx). I became closer with HMTL and CSS. And we used DotNetNuke cms for one of the project. Team used JIRA for tasks management.</p>\n                </div>")
                    },
                ],
                education: [
                    {
                        startDate: moment('2002'),
                        endDate: moment('2007'),
                        university: 'Khmelnitsky National University',
                        degree: 'Information Technologies of Design'
                    }
                ],
                skills: [
                    {
                        name: 'C#',
                        level: 7,
                    },
                    {
                        name: 'ASP.NET MVC',
                        level: 6,
                    },
                    {
                        name: 'Visual Studio',
                        level: 8,
                    },
                    {
                        name: 'LINQ',
                        level: 5,
                    },
                    {
                        name: 'Entity Framework',
                        level: 4,
                    },
                    {
                        name: 'Web API',
                        level: 3,
                    },
                    {
                        name: 'WCF',
                        level: 2,
                    },
                    {
                        name: 'SQL',
                        level: 7,
                    },
                    {
                        name: 'MS SQL Server',
                        level: 9,
                    },
                    {
                        name: 'MySql',
                        level: 5,
                    },
                    {
                        name: 'Ajax',
                        level: 4,
                    },
                    {
                        name: 'jQuery',
                        level: 3,
                    },
                    {
                        name: 'HTML',
                        level: 2,
                    },
                    {
                        name: 'CSS',
                        level: 1,
                    },
                    {
                        name: 'Bootstrap',
                        level: 8,
                    },
                    {
                        name: 'Materialize',
                        level: 7,
                    },
                    {
                        name: 'Less',
                        level: 5,
                    },
                    {
                        name: 'TFS',
                        level: 3,
                    },
                    {
                        name: 'SVN',
                        level: 1,
                    },
                    {
                        name: 'Git',
                        level: 7,
                    },
                    {
                        name: 'Jira',
                        level: 5,
                    },
                    {
                        name: 'Angular',
                        level: 6,
                    },
                    {
                        name: 'Knockout',
                        level: 4,
                    },
                    {
                        name: 'JavaScript',
                        level: 3,
                    },
                ]
            },
        ];
    }
    CurriculumService.prototype.getPersons = function () {
        return this.persons;
    };
    CurriculumService.prototype.getPerson = function (id) {
        return this.persons.find(function (x) { return x.id == id; });
    };
    CurriculumService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [DomSanitizer])
    ], CurriculumService);
    return CurriculumService;
}());
export { CurriculumService };
//# sourceMappingURL=curriculum.service.js.map