﻿namespace UsersAPI.Models
{
    public class EmailConfirmationModel
    {
        public string Email { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }
    }
}
