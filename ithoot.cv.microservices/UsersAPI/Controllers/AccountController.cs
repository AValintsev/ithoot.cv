﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using UsersAPI.Helpers;
using UsersAPI.Models;

namespace UsersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailSender<EmailConfirmationModel> _emailSender;

        public AccountController(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            IConfiguration configuration,
            IEmailSender<EmailConfirmationModel> emailSender)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _configuration = configuration;
            _emailSender = emailSender;
        }

        [HttpPost, Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var user = new IdentityUser
            {
                UserName = model.UserName,
                Email = model.Email
            };
            var existedUser = _userManager.Users.FirstOrDefault(u => u.UserName.Equals(user.UserName) || u.Email.Equals(user.Email));

            if (existedUser != null)
            {
                if (existedUser.Email.Equals(user.Email))
                    return Ok($"User with email {user.Email} already registered");
                else
                    return Ok($"Username {user.UserName} already taken");
            }

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                await SendConfirmationEmail(user.Id, model.Email);
                var token = GenerateJwtToken(user);

                return Ok(new { Token = token });
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        [HttpPost, Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);

            if (result.Succeeded)
            {
                var appUser = _userManager.Users.SingleOrDefault(user => user.UserName == model.UserName);

                if (appUser != null && !appUser.EmailConfirmed)
                {
                    return Ok("Check email and confirm registration.");
                }

                var token = GenerateJwtToken(appUser);

                return Ok(new { Token = token});
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpGet, Route("confirm")]
        public async Task<IActionResult> ConfirmEmail([FromQuery]string userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

            if (user == null)
            {
                return BadRequest();
            }
            else if (!user.EmailConfirmed)
            {
                user.EmailConfirmed = true;

                var result = await _userManager.UpdateAsync(user);

                if (!result.Succeeded)
                {
                    return BadRequest();
                }
            }

            return Ok();
        }

        private string GenerateJwtToken(IdentityUser user)
        {
            var claimList = new List<Claim>
            {
                new Claim("Email", user.Email),
                new Claim("UserName", user.UserName)
            };

            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["AppSettings:JwtSecretKey"]));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var tokenOptions = new JwtSecurityToken(
                issuer: _configuration["AppSettings:JwtIssuer"],
                audience: _configuration["AppSettings:JwtIssuer"],
                claims: claimList,
                expires: DateTime.Now.AddHours(12),
                signingCredentials: signinCredentials
            );

            var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);

            return tokenString;
        }

        private async Task SendConfirmationEmail(string id, string email)
        {
            Request.Headers.TryGetValue("Origin", out var originValues);
            await _emailSender.SendEmailAsync(new EmailConfirmationModel
            {
                Email = email,
                Subject = "Account Confirmation",
                Message = $"To confirm registration follow link: {originValues.First()}/login?userId={id}"
            });
        }
    }
}