﻿using System.Threading.Tasks;

namespace UsersAPI.Helpers
{
    public interface IEmailSender<T>
    {
        Task<bool> SendEmailAsync(T model);
    }
}
