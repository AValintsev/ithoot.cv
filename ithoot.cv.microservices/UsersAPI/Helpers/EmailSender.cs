﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;
using System.Threading.Tasks;
using UsersAPI.Models;

namespace UsersAPI.Helpers
{
    public class EmailSender : IEmailSender<EmailConfirmationModel>
    {
        private readonly IConfiguration _config;

        private const string _companyEmailKey = "AppSettings:CompanyEmail";
        private const string _companyEmailPasswordKey = "AppSettings:CompanyEmailPassword";
        private const string _emailServerKey = "AppSettings:EmailServer";
        private const string _emailServerPortKey = "AppSettings:EmailServerPort";
        private const string _companyNameKey = "AppSettings:CompanyName";

        public EmailSender(IConfiguration configuration)
        {
            _config = configuration;
        }

        public async Task<bool> SendEmailAsync(EmailConfirmationModel model)
        {
            MimeMessage message = new MimeMessage();

            message.From.Add(new MailboxAddress(_config[_companyNameKey], _config[_companyEmailKey]));
            message.To.Add(new MailboxAddress(model.Email));
            message.Subject = model.Subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = model.Message
            };

            using (SmtpClient client = new SmtpClient())
            {
                await client.ConnectAsync(_config[_emailServerKey], int.Parse(_config[_emailServerPortKey]), true);
                await client.AuthenticateAsync(_config[_companyEmailKey], _config[_companyEmailPasswordKey]);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }

            return true;
        }
    }
}
